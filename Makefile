CC = gcc -Wall -c
san = -fsanitize=address,undefined

bin/cola: obj/main.o obj/linkedList.o
	gcc $^ $(san) -o $@

obj/main.o: src/main.c
	$(CC) $^ -I include/ -o $@

obj/linkedList.o: src/linkedList.c
	$(CC) $^ -o $@

.PHONY : clean

clean : 
	rm obj/*
	rm bin/*