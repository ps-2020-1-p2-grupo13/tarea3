#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"

int main() {
	cola c;
	crear_cola(&c);
	int answer;
	char entrada[64];

	while (1)
	{	
		
		fgets (entrada, sizeof(entrada), stdin);
		sscanf (entrada, "%d", &answer);
		if (strcmp(entrada,"x\n")==0)
		{	
			break;
		}else
		{
			int ent = atoi(entrada);
			encolar(&c,(int *)ent);
		}	
	}

	printf("La cola contiene: %ld\n", tamano_cola(&c));

	printf("Elemento que desea buscar: ");
	
	fgets (entrada, sizeof(entrada), stdin);
	sscanf (entrada, "%d", &answer);
	printf("\nEl elemento esta en la posicion: %ld\n",posicion_cola(&c,atoi(entrada)));
	
	printf("Elemento sacado:%d\n",(int *)decolar(&c));
	
	decolar(&c);

	destruir_cola(&c);

	return 0;
}
//Pedro Conforme , Xavier Magallanes
