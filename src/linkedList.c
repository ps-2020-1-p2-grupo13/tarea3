#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LinkedList.h"

void crear_cola(cola *cola){
    cola->tamano = 0;
    cola->inicio = NULL;
    cola->fin = NULL;
}

int encolar(cola *mi_cola, void *elemento){
    nodo_colaTDA *temp;
    temp = malloc(sizeof(nodo_colaTDA));
    temp->siguiente = NULL;
    temp->elemento = elemento;
    if(isEmpty(mi_cola) == 1){
        mi_cola->fin = mi_cola->inicio = temp;
    }else{
        mi_cola->fin->siguiente = temp;
        temp->anterior = mi_cola->fin;
        temp->siguiente = NULL;
        mi_cola->fin = temp;
    }
    mi_cola->tamano++;


}

void *decolar(cola *mi_cola){
    nodo_colaTDA *temp;
    if(mi_cola->tamano == 1){
        void *elemento = mi_cola->fin->elemento;
        temp = mi_cola->fin;
        mi_cola->inicio = NULL;
        mi_cola->fin = NULL;
        mi_cola->tamano = 0;
        free(temp);
        return elemento;
    }
    else if(mi_cola->tamano > 1){
        void *elemento = mi_cola->inicio->elemento;
        temp = mi_cola->inicio;
        mi_cola->inicio = mi_cola->inicio->siguiente;
        mi_cola->inicio->anterior = NULL;
        mi_cola->tamano--;
        free(temp);
        return elemento;
    }
    else{
        printf("La cola esta vacia\n");

    }
}

unsigned long tamano_cola(cola *mi_cola){
    return mi_cola->tamano;
}

unsigned long posicion_cola(cola *mi_cola, void *elemento){
    nodo_colaTDA *temp = mi_cola->inicio;
    unsigned long posicion = 0;

    
    while (temp->elemento != NULL)
    {
        if(elemento == temp->elemento)
        {
            return posicion;
        }
        temp = temp->siguiente;
        posicion ++;

    }


    return 0;


}

int destruir_cola(cola *mi_cola){
    nodo_colaTDA *temp = mi_cola->inicio;
    unsigned long posicion = 0;
    if(mi_cola->tamano != 0){
        while (temp->siguiente != NULL){
            free(temp);
            temp = temp->siguiente;

        }
        free(mi_cola->fin);
    }else{
        printf("La cola esta vacia\n");
        return 1;
    }
    return 1;
}

int isEmpty(cola *mi_cola){
    return mi_cola->tamano == 0;
}

//Pedro Conforme , Xavier Magallanes
